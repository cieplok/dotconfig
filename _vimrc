if has('gui_running')
    set guifont=Consolas:h12
endif

set encoding=utf-8
set langmenu=en_US.UTF-8 

set modelines=1

let mapleader = "\<Space>"

" NeoBundle {{{
if has('vim_starting')
    set nocompatible
    set runtimepath+=~/.vim/bundle/neobundle.vim
endif

call neobundle#begin(expand('~/.vim/bundle/'))

NeoBundleFetch 'Shougo/neobundle.vim'

NeoBundle 'bling/vim-airline'
NeoBundle 'mbbill/undotree'
NeoBundle 'tpope/vim-vinegar'
NeoBundle 'tpope/vim-dispatch'
NeoBundle 'tpope/vim-unimpaired'
NeoBundle 'tpope/vim-repeat'
NeoBundle 'tpope/vim-commentary'
NeoBundle 'tpope/vim-surround'
NeoBundle 'tpope/vim-rsi'

NeoBundle 'kien/ctrlp.vim'
NeoBundle 'rking/ag.vim'

NeoBundle 'altercation/vim-colors-solarized'
NeoBundle 'sjl/badwolf'

NeoBundle 'pangloss/vim-javascript'
NeoBundle 'groenewege/vim-less'
NeoBundle 'vim-coffee-script'
NeoBundle 'hail2u/vim-css3-syntax'
"NeoBundle 'jelera/vim-javascript-syntax'
NeoBundle 'tmhedberg/matchit'
NeoBundle 'wellle/targets.vim'

NeoBundle 'Shougo/neocomplete.vim'

NeoBundle 'haya14busa/incsearch.vim'
NeoBundle 'haya14busa/incsearch-fuzzy.vim'

NeoBundle 'unblevable/quick-scope'

NeoBundle 'maxbrunsfeld/vim-yankstack'

call neobundle#end()

filetype plugin indent on
" }}}

" Text settings {{{
" Enable syntax highlighting
syntax enable

" Use spaces instead of tabs
set expandtab

" Be smart when using tabs ;)
set smarttab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

" from sensible.vim
set backspace=indent,eol,start
set complete-=i
set showmatch

set nrformats-=octal
set shiftround

set ttimeout
set ttimeoutlen=50

set wrap linebreak nolist 

" Don't set cursors on begging of the line after buffer switch
set nostartofline
" }}}

" Search settings {{{
set ignorecase
set smartcase
set hlsearch
set incsearch
set cursorline

set ai "Auto indent
set si "Smart indent
" }}}

" Display settings {{{
"Always show current position
set ruler

" ladne numerki po lewej stronie
set number relativenumber

set laststatus=2
set wildmenu

if !&scrolloff
    set scrolloff=7
endif
if !&sidescrolloff
    set sidescrolloff=7
endif

if &history < 1000
    set history=1000
endif
if &tabpagemax < 50
    set tabpagemax=50
endif
" }}}

" Autoload {{{
set autochdir

" brak backapow
set nobackup
set noswapfile

au GUIEnter * simalt ~x

set autoread
" }}}

" Colors {{{
if has('gui_running')
    set background=dark
    colorscheme badwolf
    let g:solarized_italic=0
else
    set background=dark
    echo 'konsola'
    colorscheme default
endif
" }}}

" Command line {{{
    nnoremap <silent> <F6> :!%<CR>
    nnoremap <silent> <F7> :!% <C-R>p<CR>

    " powershell make
    set makeprg=powershell\ -executionpolicy\ bypass\ -file\ %:p
    set autowrite
    nnoremap <F5> :make <bar> copen<CR>
    "
    " scriptcs
    nnoremap <silent> <leader>cs :!scriptcs %<CR>
    nnoremap <leader>lp :!lprun -lang=program %<CR>
    nnoremap <leader>lpe :!lprun -lang=e %<CR>

    " file types
    au BufRead,BufNewFile *.xaml setfiletype xml
    au BufRead,BufNewFile *.msbuild setfiletype xml
    au BufRead,BufNewFile *.targets setfiletype xml
    au BufRead,BufNewFile *.csx setfiletype cs
    
" }}}

" Leader mappings {{{
    " edycja i ladowanie pliku .vimrc {{{
    nnoremap <leader>ev :split $MYVIMRC<cr>
    nnoremap <leader>sv :source $MYVIMRC<cr>

    nnoremap <silent><leader>cl :noh<CR>
    " }}}

    " xml format {{{
    nmap <silent> <leader>x :%!xmllint --format --recover -<CR>
    nmap <silent> <leader>del :g/^\s*$/d<CR>
    nmap <silent> <leader>rm :%s/<c-v><c-m>//g<CR>
    " }}}

    " format all {{{
    nnoremap <leader>fa mzgg=G`z
    nnoremap <leader>fb mzvi{=`z
    " }}}

    " json pretty print {{{
    nnoremap <leader>jf :%!python -mjson.tool<CR>:setf json<CR>
    nnoremap <leader>rj ggVG"+p:%!python -mjson.tool<CR>:setf javascript<CR>
    " }}}

    " sciezki windows {{{
    nnoremap <silent> <leader>fp :s:\\:\/:g<CR>:nohlsearch<CR>
    vnoremap <silent> <leader>fp :s:\\:\/:g<CR>:nohlsearch<CR>
    nnoremap <silent> <leader>cfp :let @+=expand("%:p")<CR>

    nnoremap <leader>sp q:avimgrep "" <C-R>+/**/*.*<Esc>F"

    nnoremap <leader>n :ene<CR>
    " }}}

    " Misc {{{
    " do recznego ustawiania czcionki
    nnoremap <silent> <leader>sf :set guifont=*<CR>

    nnoremap <leader>o o<Esc>

    " search for selection
    vnoremap <leader>ff yq/p<CR>
    nnoremap <leader>rp :%s//<C-R>+<CR>

    nnoremap <silent> <C-f>r /\v<C-R>
    nnoremap <silent> <C-f>c /\v<C-R>+<CR>
    nnoremap <silent> <C-f>y /\v<C-R>0<CR>

    nnoremap <leader>a ggVG

    nnoremap j gj
    nnoremap k gk
    nnoremap gj j
    nnoremap gk k

    nnoremap <silent> <leader>cc :q!<CR>
    nnoremap <silent> <leader>ss :w!<CR>

    nnoremap <silent> <leader>q q:
    " }}}
    
    " Windows {{{
    nnoremap <leader>ww <C-W><C-W>
    nnoremap <leader>wl <C-W><C-L>
    nnoremap <leader>wk <C-W><C-K>
    nnoremap <leader>wj <C-W><C-J>
    nnoremap <leader>wh <C-W><C-H>

    nnoremap <leader>w= <C-W>=
    nnoremap <leader>w_ <C-W>_
    nnoremap <leader>wf <C-W>_
    " }}}

    " Buffers {{{
        map <C-tab> :bn<cr>
        map <C-S-tab> :bp<cr>
    " }}}
    "
    nnoremap <space><space> :

" }}}

" Copy and paste {{{
nnoremap <leader>p "+p
nnoremap <leader>P "+P
vnoremap <leader>p "+p
vnoremap <leader>P "+P
vnoremap <leader>l "+gp
vnoremap <leader>y "+y
"vnoremap <leader>yiw viw"+y

nnoremap <Space>0 "0p
vnoremap <Space>0 "0p

" przepisz ostatnio skopiowane wyrazenie do schowka
" nnoremap <silent> <leader>cc :let @+=@0<CR>
" }}}

" Relative numbers {{{
function! NumberToggle()
    if(&relativenumber == 1)
        set number
    else
        set relativenumber
    endif
endfunc

nnoremap <F2> :call NumberToggle()<CR>
" }}}

" Airline {{{
let g:airline_theme='molokai'
let g:airline_section_b = '%{getcwd()}'
let g:airline_section_b = '%{getcwd()}'
let g:airline#extensions#default#section_truncate_width = { 'b' : 60 }
let g:airline#extensions#tabline#enabled = 1
" }}}

" Folding {{{
set foldmethod=syntax
set nofoldenable
set foldlevelstart=99

let javaScript_fold=1         " JavaScript
let perl_fold=1               " Perl
let php_folding=1             " PHP
let r_syntax_folding=1        " R
let ruby_fold=1               " Ruby
let sh_fold_enabled=1         " sh
let vimsyn_folding='af'       " Vim script
let xml_syntax_folding=1      " XML

au FileType xml setlocal foldmethod=syntax

" allow switch from unsaved buffer
set hidden

" preserve folding when changing buffers
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview
" }}}

" Misc settings {{{
" wylaczenie kolorowania jesli duze pliki
"autocmd BufReadPre * if getfsize(expand("%")) > 10000000 | syntax off | endif

" Use sane regexes.
nnoremap / /\v
vnoremap / /\v

"przyspieszenie wykonywania makr przez nie-odrysowywanie za kazdym razem
set lazyredraw

" ograniczenie ilosci kolorowanych linii - powinno przyspieszyc odrysowawywanie
set synmaxcol=1200

let g:netrw_liststyle=3
" }}}

" Undotree {{{
nnoremap <F3> :UndotreeToggle<CR>
" }}}

" Buffers {{{
nnoremap <silent> <C-q> :bp<bar>bd! #<CR>
nnoremap <C-S> :b #<CR>
" }}}

" CtrlP {{{
let g:ctrlp_root_markers = ['*.sln', '*.sbt', 'Gruntfile.js']
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_clear_cache_on_exit=0
let g:ctrlp_extensions = ['quickfix']
let g:ctrlp_mruf_case_sensitive = 0
let g:ctrlp_mruf_max = 1000
let g:ctrlp_show_hidden = 1
let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git'
nnoremap <silent> <C-m> :CtrlPMRU<CR>
nnoremap <silent> <C-b> :CtrlPBuffer<CR>
nnoremap <silent> <C-l> :CtrlPLastMode<CR>
nnoremap <silent> <C-f>l :CtrlPLine<CR>
nnoremap <silent> <leader>fq :CtrlPQuickfix<CR>
" }}}

" IncSearch {{{

map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)

" :h g:incsearch#auto_nohlsearch
let g:incsearch#auto_nohlsearch = 1
map n  <Plug>(incsearch-nohl-n)
map N  <Plug>(incsearch-nohl-N)
map *  <Plug>(incsearch-nohl-*)
map #  <Plug>(incsearch-nohl-#)
map g* <Plug>(incsearch-nohl-g*)
map g# <Plug>(incsearch-nohl-g#)

map z/ <Plug>(incsearch-fuzzy-/)
map z? <Plug>(incsearch-fuzzy-?)
map zg/ <Plug>(incsearch-fuzzy-stay)

" }}}

" QuickScope {{{
" Insert into your .vimrc after quick-scope is loaded.
" Obviously depends on <https://github.com/unblevable/quick-scope> being installed.

function! Quick_scope_selective(movement)
    let needs_disabling = 0
    if !g:qs_enable
        QuickScopeToggle
        redraw
        let needs_disabling = 1
    endif

    let letter = nr2char(getchar())

    if needs_disabling
        QuickScopeToggle
    endif

    return a:movement . letter
endfunction

let g:qs_enable = 0

nnoremap <expr> <silent> f Quick_scope_selective('f')
nnoremap <expr> <silent> F Quick_scope_selective('F')
nnoremap <expr> <silent> t Quick_scope_selective('t')
nnoremap <expr> <silent> T Quick_scope_selective('T')
vnoremap <expr> <silent> f Quick_scope_selective('f')
vnoremap <expr> <silent> F Quick_scope_selective('F')
vnoremap <expr> <silent> t Quick_scope_selective('t')
vnoremap <expr> <silent> T Quick_scope_selective('T')" 

"}}}

" Ag {{{

    let g:ag_working_path_mode="r"
    let g:ag_highlight=1
    let g:ag_prg="ag --smart-case --column --stats"

    nnoremap \ :Ag<SPACE>
    nnoremap <leader>\ :AgFromSearch<CR>
    vnoremap <leader>\ y:Ag<SPACE><C-R>"<CR>
    nnoremap <silent> <leader>ff :copen<CR>

" }}}

" yankstack {{{

    nnoremap <a-n> <Plug>yankstack_substitute_older_paste
    nnoremap <a-s-n> <Plug>yankstack_substitute_newer_paste
    nnoremap <a-m> :Yanks<CR>
    
" }}}

" vim:foldmethod=marker:foldlevel=0
